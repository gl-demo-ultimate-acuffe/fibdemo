package fibber

import (
	"context"
	"fmt"
	"math/big"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	MaximumIntFibN = 93
)

type FibberT struct {
	cache map[int]string

	lookupCount *prometheus.CounterVec
}

func New(registry prometheus.Registerer) *FibberT {
	res := &FibberT{
		// Limit is arbitrary
		cache: make(map[int]string),
	}

	res.lookupCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "lookups_total",
			Help: "number of fibonacci lookups",
		},
		[]string{"from_cache"},
	)
	registry.MustRegister(res.lookupCount)

	return res
}

func (fb *FibberT) Lookup(ctx context.Context, n int) string {
	var from_cache bool
	defer func() {
		fb.lookupCount.WithLabelValues(fmt.Sprintf("%t", from_cache)).Inc()
	}()

	res, ok := fb.lookupCache(ctx, n)
	if ok {
		from_cache = true
		return res
	}

	if n < MaximumIntFibN {
		res = strconv.FormatInt(fb.calculateSmall(ctx, n), 10)
	} else {
		res = fb.calculateBig(ctx, n)
	}

	// For the sake of presentation/workshop, we intentionally do not cache
	// intermediate values when calculating fibonacci numbers
	fb.setCache(ctx, n, res)
	return res
}

func (fb *FibberT) lookupCache(ctx context.Context, n int) (string, bool) {
	val, ok := fb.cache[n]
	return val, ok
}

func (fb *FibberT) setCache(ctx context.Context, n int, val string) {
	fb.cache[n] = val
}

func (fb *FibberT) calculateSmall(ctx context.Context, n int) int64 {
	switch {
	case n == 0:
		return 0
	case n == 1:
		return 1
	default:
		cur, prev := int64(1), int64(1)
		for i := 2; i < n; i++ {
			cur, prev = prev+cur, cur
		}
		return cur
	}
}

func (fb *FibberT) calculateBig(ctx context.Context, n int) string {
	cur := big.NewInt(fb.calculateSmall(ctx, MaximumIntFibN-1))
	prev := big.NewInt(fb.calculateSmall(ctx, MaximumIntFibN-2))
	for i := MaximumIntFibN; i <= n; i++ {
		prev.Add(prev, cur)
		cur, prev = prev, cur
	}
	return cur.String()
}
