package instrumentation

// Based upon https://github.com/penglongli/gin-metrics

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
)

// Use set gin metrics middleware.
func RouteMetricsOtel(m metric.Meter) gin.HandlerFunc {
	ginMetricHandle := initGinMetricsOtel(m)

	return func(ctx *gin.Context) {
		startTime := time.Now()
		ctx.Next()
		latency := time.Since(startTime)

		ginMetricHandle(ctx, latency)
	}
}

// initGinMetrics used to init gin metrics.
func initGinMetricsOtel(m metric.Meter) func(*gin.Context, time.Duration) {
	metricURIRequestTotal, err := m.Int64Counter(
		"gin_uri_request_total",
		metric.WithDescription("all the server received request num with every uri"),
	)
	if err != nil {
		panic(fmt.Sprintf("unable to create `gin_uri_request_total` Int64Counter: %v", err))
	}

	metricRequestBody, err := m.Int64Counter(
		"gin_request_body_total",
		metric.WithDescription("the server received request body size"),
		metric.WithUnit("byte"),
	)
	if err != nil {
		panic(fmt.Sprintf("unable to create `gin_request_body_total` Int64Counter: %v", err))
	}

	metricResponseBody, err := m.Int64Counter(
		"gin_response_body_total",
		metric.WithDescription("the server send response body size"),
		metric.WithUnit("byte"),
	)
	if err != nil {
		panic(fmt.Sprintf("unable to create `gin_response_body_total` Int64Counter: %v", err))
	}

	metricRequestDuration, err := m.Float64Histogram(
		"gin_request_duration",
		metric.WithDescription("the time server took to handle the request"),
		metric.WithUnit("second"),
	)
	if err != nil {
		panic(fmt.Sprintf("unable to create `gin_request_duration` Int64Counter: %v", err))
	}

	return func(ctx *gin.Context, latency time.Duration) {
		r := ctx.Request
		w := ctx.Writer

		// set uri request total
		metricURIRequestTotal.Add(
			ctx,
			1,
			metric.WithAttributes(
				attribute.String("uri", ctx.FullPath()),
				attribute.String("method", r.Method),
				attribute.Int("code", w.Status()),
			),
		)

		// set request body size
		// since r.ContentLength can be negative (in some occasions) guard the operation
		if r.ContentLength >= 0 {
			metricRequestBody.Add(
				ctx,
				r.ContentLength,
				metric.WithAttributes(
					attribute.String("uri", ctx.FullPath()),
					attribute.String("method", r.Method),
					attribute.Int("code", w.Status()),
				),
			)
		}

		// set response size
		if w.Size() > 0 {
			metricResponseBody.Add(
				ctx,
				int64(w.Size()),
				metric.WithAttributes(
					attribute.String("uri", ctx.FullPath()),
					attribute.String("method", r.Method),
					attribute.Int("code", w.Status()),
				),
			)
		}

		// set request duration
		metricRequestDuration.Record(
			ctx,
			latency.Seconds(),
			metric.WithAttributes(
				attribute.String("uri", ctx.FullPath()),
			),
		)
	}
}
