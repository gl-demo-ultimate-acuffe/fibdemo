SHELL=/bin/bash
.SHELLFLAGS=-euo pipefail -c

OS       := $(shell uname -s | tr '[:upper:]' '[:lower:]')
ARCH     := $(shell uname -m)

TEMP_DIR:=$(shell mktemp -d)
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

ifeq ($(ARCH),x86_64)
	ARCH := amd64
endif

ifeq ($(ARCH),armv7l)
	ARCH := armhf
endif

ifeq ($(ARCH),aarch64)
	ARCH := arm64
endif

VER_traefik = v3.0.0-rc1
URL_traefik = https://github.com/traefik/traefik/releases/download/${VER_traefik}/traefik_${VER_traefik}_$(OS)_$(ARCH).tar.gz

install-traefik: $(outdir)bin/traefik
$(outdir)bin/traefik:
	mkdir -pv bin/
	curl -fSL ${URL_traefik} -o $(TEMP_DIR)/traefik.tar.gz
	tar -xvzf $(TEMP_DIR)/traefik.tar.gz -C bin/ traefik

.PHONY: run-fibber
run-fibber:
	go run ./cmd/fib/main.go

.PHONY: run-traefik
run-traefik: install-traefik
	bin/traefik \
		--global.sendanonymoususage=false \
		--global.checknewversion=false \
		--entrypoints.traefik=true \
		--entrypoints.traefik.address=:9079/tcp \
		--entrypoints.web=true \
		--entrypoints.web.address=:9080/tcp \
		--entrypoints.web.asdefault=true \
		--api=true \
		--api.dashboard=true \
		--api.insecure=true \
		--accesslog=true \
		--providers.file.directory=$(ROOT_DIR)/conf/ \
		--log.level=INFO
